#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <locale.h>
#define N 10
int i = 0, j = 0;
struct BOOK
{
	char title[256];
	char author[256];
	int year;
};
int read(FILE *file, struct BOOK *book)
{
	char arr[256];
	for (i = 0; i < N; i++)
	{
		fgets(book[i].title, 256, file);
		fgets(book[i].author, 256, file);
		fgets(arr, 256, file);
		book[i].year = 0;
		j = 0;
		while (arr[j] != '\n')
		{
			book[i].year = book[i].year * 10 + arr[j] - '0';
			j++;
		}
		printf("%s %s %d\n", book[i].title, book[i].author, book[i].year);
	}
	return book;
}

void OldNew(struct BOOK *book)
{
	int min = book[0].year, max = book[0].year;
	for (i = 0; i < N; i++)
	{
		if (book[i].year != min || book[i].year != max)
		{
			if (book[i].year < min)
				min = book[i].year;
			if (book[i].year > max)
				max = book[i].year;
		}
	}
	printf("min = %d, max = %d\n", min, max);
}

void Sort(struct BOOK *book)
{
	int temp = 0;
	struct BOOK loop;
	for (i = 0; i < N - 1; i++)
	{
		for (j = i + 1; j < N; j++)
		{
			temp = strcmp(book[i].author, book[j].author);
			if (temp > 0)
			{
				loop = book[i];
				book[i] = book[j];
				book[j] = loop;
			}
		}
	}
	for (i = 0; i < N; i++) 
		printf("%s\t%s\t%i\n", book[i].title, book[i].author, book[i].year);
}
int main()
{
	FILE *file;
	struct BOOK *book = (struct BOOK*)malloc(sizeof(struct BOOK));
	file = fopen("D:\\labs\\pi-tasks-05\\task05-1\\input.txt", "r");
	if (file == 0)
	{
		printf("Error\n");
		return(1);
	}
	setlocale(LC_ALL, "Rus");
	
	read(file, book);
	OldNew(book);
	Sort(book);
	fclose(file);
	return 0;
}