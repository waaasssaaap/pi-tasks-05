#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 128

struct BOOK
{
	char name[N];
	char author[N];
	int date;
};

int qtStrings(FILE* fp)	//�-�� �������� ���-�� ����� � txt
{
	int qt=0;
	char buf[N];
	while (fgets(buf, N, fp))
		qt++;
	rewind(fp);
	if ((qt%3!=0) || (qt==0)) return NULL;
	return qt;
}

void getBooks(FILE *fp, struct BOOK *books, int qt)
{
	int i, j=0;
	char buf[N];
	for (i=0; i<qt/3; i++)
	{
		fgets(books[i].name, N, fp);
		fgets(books[i].author, N, fp);
		fgets(buf, N, fp);
		books[i].date=atoi(buf);
	}
}

void SortOfAuthor(struct BOOK *books, int qt)
{
	int i, j, minInd=0;
	printf("Sorted information about books:\n\n");
	for (j=0; j<qt/3; j++)
	{
		for (i=0; i<qt/3-j; i++)
			if (strcmp(books[i].author, books[minInd].author) < 0)
				minInd=i;
		printf("%d)%s  %s  %d\n\n", j+1, books[minInd].name, books[minInd].author, books[minInd].date);
		strcpy(books[minInd].name, books[qt/3-j-1].name);		//����������� ��������� ��������� �� ����� ���������� ���������
		strcpy(books[minInd].author, books[qt/3-j-1].author);	//����� ���� ����������� ��������� ������ ������, ����� �� ���������� ��� ���
		books[minInd].date=books[qt/3-j-1].date;				//�� �����, ��� ��� ������� ���-�� �������� ��� ����� �������������
		minInd=0;
	}
}

int main()
{
	FILE* fp;
	int qtStr;
	struct BOOK *books;
	if ((fp=fopen("C:\\Users\\������������\\Desktop\\task044\\Debug\\books.txt", "r"))==NULL)	
	{
		perror("Error: ");
		return 1;
	}
	qtStr=qtStrings(fp);
	if (qtStr==NULL)
	{
		printf("Error: check the correction of information in your text file.\n");
		return 1;
	}
	books=(struct BOOK*)malloc((qtStr/3)*sizeof(struct BOOK));	//��������� ������ � ������ ���-�� ����
	getBooks(fp, books, qtStr);
	SortOfAuthor(books, qtStr);
	fclose(fp);
	free(books);
	return 0;
}
