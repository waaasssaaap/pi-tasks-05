﻿#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 256
FILE *fp;
typedef struct BOOK Book;
struct BOOK {
	char title[N];
	char author[N];
    int year;
};
void readbook(char a[], Book *b){
	int i = 0, j = 0;
	char year[N];
	while (a[i] != ',')
		b->title[j++] = a[i++];
	b->title[j] = 0;
	j = 0; i++;
	while (a[i] != ',')
		b->author[j++] = a[i++];
	b->author[j] = 0;
	j = 0; i++;
	while (a[i] != 0)
		year[j++] = a[i++];
	year[j] = 0;
	b->year = atoi(year);
}
int main(){
	Book *arr, old_y, young_y, tmp;
	fpos_t position;
	char booknow[N];
	int i, j, old_year, young_year, numbooks = 0;
	fp = fopen("BOOK.txt", "r+");
	if (fp == NULL){
		perror("File");
		return 1;
	}
	arr = (Book*)malloc(numbooks*sizeof(Book));
	fgetpos(fp, &position);
	while (fgets(booknow, N, fp))
		numbooks++;
	fsetpos(fp, &position);
	for (i = 0; i < numbooks; i++){
		fgets(booknow, N, fp);
		readbook(booknow, &arr[i]);
	}
	while (fgets(arr, N, fp) != NULL)
		printf("%s", arr);
	printf("\n");
	old_year = young_year = arr[0].year;
	old_y = arr[0];
	young_y = arr[0];
	for (i = 1; i < numbooks; i++)
	if (arr[i].year > old_year){
		old_year = arr[i].year;
		old_y = arr[i];
	}
	else if (arr[i].year < young_year){
		young_year = arr[i].year;
		young_y = arr[i];
	}
	for (i = 0; i < numbooks - 1; i++)
	for (j = 0; j < numbooks - i - 1; j++)
	if (strcmp(arr[j].author, arr[j + 1].author) > 0){
		tmp = arr[j];
		arr[j] = arr[j + 1];
		arr[j + 1] = tmp;
	}
	puts("The youngest book of the year edition:");
	printf("\"%s\" %s, %d\n", old_y.title, young_y.author, old_y.year);
	puts("The oldest book of the year edition:");
	printf("\"%s\" %s, %d\n", young_y.title, old_y.author, young_y.year);
	puts("Sorted books by last names author:");
	for (i = 0; i < numbooks; i++)
		printf("\"%s\" %s, %d\n", arr[i].title, arr[i].author, arr[i].year);
	return 0;
}