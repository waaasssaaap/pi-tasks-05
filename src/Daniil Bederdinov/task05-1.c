#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define N 50
#define M 5

FILE *fp;
typedef struct
{
    char title[N];
    char author[N];
    int year;
} BOOK;

void print(BOOK *catalog, int count)
{
    for (int i = 0; i <= count; i++)
        printf("%s - %s, %d\n", catalog[i].title, catalog[i].author, catalog[i].year);
    puts("");
}
void swap(BOOK *catalog, int book1, int book2)
{
    BOOK temp;
    temp = catalog[book1];
    catalog[book1] = catalog[book2];
    catalog[book2] = temp;
}
int main()
{
    BOOK *catalog;
    int count = 0;
    int i = 0;
    int j=0;
    int bookslimit = M;
    int oldOne = 0;
    int newOne = 0;
    int smallest = 0;
    catalog = (BOOK *)malloc(bookslimit*(sizeof(BOOK)));
    if (catalog == NULL)
    {
        printf("Allocation problems \n");
        return 1;
    }
    fp = fopen("books.txt", "r");
    if (fp == NULL)
    {
        perror("File error");
        return 1;
    }
    while (fscanf(fp, "%s%s%d", &catalog[i].title, &catalog[i].author, &catalog[i].year) != EOF)
    {
        i++;
        if (i == bookslimit)
        {
            bookslimit += 5;
            realloc(catalog, (bookslimit * sizeof(BOOK)));
            if (catalog == NULL)
            {
                printf("Allocation problems \n");
                return 1;
            }
        }
    }
    fclose(fp);
    count = i - 1;
    print(catalog, count);

    for (i = 0; i <= count; i++)
    {
        if (catalog[i].year > catalog[newOne].year)
            newOne = i;
        if (catalog[i].year < catalog[oldOne].year)
            oldOne = i;
    }
    printf("The newest book: %s - %s, %d \n", catalog[newOne].title, catalog[newOne].author, catalog[newOne].year);
    printf("The oldest book: %s - %s, %d \n", catalog[oldOne].title, catalog[oldOne].author, catalog[oldOne].year);
    puts("");

    for (i = 0; i < count; i++)
    {
        for (j = i; j <= count; j++)
            if (strcmp(catalog[j].author, catalog[smallest].author) < 0)
                smallest = j;
        swap(catalog, i, smallest);
    }
    print(catalog, count);
    free(catalog);
    return 0;
}