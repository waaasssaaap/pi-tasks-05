﻿#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 255
#define get_into_buf fgets(buf,MAX,fp)
#define buff_to_zero buf[MAX]={0}
const int upConst = 10; // Начальное количество элементов для структуры
struct BOOK
{
	char book_name[MAX];
	char author_f[MAX];
	char author_i[MAX]; 
	char author_o[MAX];
    	int year;
};

void swap(struct BOOK *b1, struct BOOK *b2) // Для сортировки по фамилии авторов меняем адресами структурки
{
	struct BOOK *temp;
	temp = b1;
	b1 = b2;
	b2 = temp;
}

int readBook(FILE *fp, struct BOOK *book_)
{
	char buf[MAX];
		buff_to_zero; get_into_buf; // Обнуляем строку и получаем ее через fgets в дефайне описал чтоб удобнее читалось
	strcpy(buf,book_->book_name);
		buff_to_zero; get_into_buf;
	strcpy(buf,book_->author_f);
		buff_to_zero; get_into_buf;
	strcpy(buf,book_->author_i);
		buff_to_zero; get_into_buf;
	strcpy(buf,book_->author_o);
		buff_to_zero; get_into_buf;
	book_->year = atoi(buf);
	return buf==NULL;
}

int main()
{
	FILE *fp;
	struct BOOK *books;
	int inc=0, curMin =0, curMax =0, minYear=20000, maxYear=-1, up = upConst, cur=0;
	fp = fopen("input.txt","r");
	if(fp!=NULL)
	{
		books = (struct BOOK*) malloc(up*sizeof(struct BOOK)); // Выделяем начальную память под up структур
		for(;;)
		{ 
			if(readBook(fp,books+cur)==0) break; // Если прочитан eof - конец цикла иначе все заносится в динамичную структурку
			inc++; // Текущее количество строк
			if(inc>=up) books = (struct BOOK*)realloc(books,(up*=2)*sizeof(BOOK)); // выделяем больше памяти в два раза если достигнуто up структур
		}
		
		// 1. Печать данные о книгах на экран, с паралельным поиском минимального и максимального года издания
		
		
		for(int i=0;i<inc;i++)
		{
			printf("%sMAX ", books[i].book_name);
			printf("%sMAX ", books[i].author_f);
			printf("%sMAX ", books[i].author_i);
			printf("%sMAX ", books[i].author_o);
			printf("%ui\n", books[i].year);
			if(curMin>books[i].year) minYear=i, curMin = books[i].year;
			if(curMax<books[i].year) maxYear=i, curMax = books[i].year;
		}
		// 2. Вывод данных о книгах минимального и максимального года издания
			printf("%sMAX ", books[minYear].book_name);
			printf("%sMAX ", books[minYear].author_f);
			printf("%sMAX ", books[minYear].author_i);
			printf("%sMAX ", books[minYear].author_o);
			printf("%ui\n", books[minYear].year);
			printf("%sMAX ", books[maxYear].book_name);
			printf("%sMAX ", books[maxYear].author_f);
			printf("%sMAX ", books[maxYear].author_i);
			printf("%sMAX ", books[maxYear].author_o);
			printf("%ui\n", books[maxYear].year);
		// 3. Сортировка по фамилиям авторов и вывод
		for(int i=0;i<inc-1;i++)
			for(int j=i+1;j<inc;j++)
			{
				int cond = strcmp(books[i].author_f,books[j].author_f);
				if(cond) swap((books+i),(books+j));
			}
			
		
	}
	else
	{
			printf("Error: File end exceeded!");
	}
	return 0;
}