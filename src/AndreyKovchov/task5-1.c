#include <stdio.h>
#include <locale.h>

typedef struct
{
	char title[50];
	char author[50];
	int year;
} book;

int main(int argc, char const *argv[])
{
	setlocale(LC_ALL,"Rus");
	int i, j;
	FILE *file = fopen("book.txt", "r");
	int lines = 0;
	char str[110];
	
	while(fgets(str, 255, file) != NULL)
		lines++;
	fclose(file);
	
	book *b = (book*)malloc(lines * sizeof(book));
	i = 0;
	file = fopen("book.txt", "r");
	while(fscanf(file, "%s%s%d", b[i].title, b[i].author, &(b[i].year)) != EOF){
		printf("%s %s %d\n", b[i].title, b[i].author, b[i].year);
		i++;
	}
	fclose(file);
	printf("\n\n");
	book min_b, max_b;
	int min = b[0].year, max = b[0].year;
	
	for(i=1; i<lines; i++){
		if(b[i].year < min){
			min_b = b[i];
			min = b[i].year;
		} else if(b[i].year > max){
			max_b = b[i];
			max = b[i].year;
		}
	}
	printf("Old: %s %s %d\n", min_b.title, min_b.author, min_b.year);
	printf("New: %s %s %d\n", max_b.title, max_b.author, max_b.year);
	
	for(i=0; i < lines-1; i++){
		for(j=0; j < lines-i-1; j++){
			if(strcmp(b[j].author, b[j+1].author) > 0){
				book t1 = b[j];
				b[j] = b[j+1];
				b[j+1] = t1;
			}
		}
	}
	printf("\n\nSorted by author:\n");
	for(i=0; i<lines; i++){
		printf("%s %s %d\n", b[i].title, b[i].author, b[i].year);
	}
	
	return 0;
}
