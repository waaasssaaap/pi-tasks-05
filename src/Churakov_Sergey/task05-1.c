#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

struct book
{
	char book_title[40];
	char author[20];
	int publ_year;
};


int main()
{
	FILE* fp;
	fp = fopen("C:\\books.txt", "r");
	if (fp == NULL)
	{
		perror("C:\\books.txt");
		return 1;
	}
	int i, n;
	char buf[60];
	struct book*arr_of_books=(struct*book)malloc(sizeof(struct book));
	i = 0;
	while (!EOF)
	{
		book*arr_of_books=(struct*book)realloc(arr_of_books,(i+1)*sizeof(struct book));
		fgets(arr_of_books[i].book_title, 256, fp);
		fgets(arr_of_books[i].author, 256, fp);
		fgets(buf, 256, fp);
		arr_of_books[i].publ_year = atoi(buf);
		i++;
	}
	n = i;
	fclose(fp);
	for (i = 0;i < n;i++)
	{
		printf("%s\n", arr_of_books[i].book_title);
		printf("%s\n", arr_of_books[i].author);
		printf("%d\n", arr_of_books[i].publ_year);
	}
	int min, max;
	min = max = 0;
	for (i = 0; i < n; i++)
	{
		if (arr_of_books[min].publ_year > arr_of_books[i].publ_year)
			min = i;
		if (arr_of_books[max].publ_year < arr_of_books[i].publ_year)
			max = i;
	}
	printf("The oldest publication:\n%s \n%s \n%d", arr_of_books[min].book_title, arr_of_books[min].author, arr_of_books[min].publ_year);
	printf("The latest publication:\n%s \n%s \n%d", arr_of_books[max].book_title, arr_of_books[max].author, arr_of_books[max].publ_year);
	int j;
	int temp, cmp;
	int arr[60];
	for (i = 0;i < n;i++)
		arr[i] = i;
	for (i = 1;i < n;i++)
	{
		j=i;
		temp = arr[i];
		cmp = strcmp(arr_of_books[i].author, arr_of_books[j - 1].author);
		while (j > 0 && cmp < 0)
		{
			arr[j] = arr[j - 1];
			j--;
		}
		arr[j] = temp;
	}
	for (i = 0;i < n;i++)
	{
		printf("%s\n", arr_of_books[arr[i]].book_title);
		printf("%s\n", arr_of_books[arr[i]].author);
		printf("%d\n", arr_of_books[arr[i]].publ_year);
	}
	Sleep(10000);
	return 0;
}