#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define NAME_LENGTH 30
#define AUTHOR_LENGTH 50
#define DEFAULT_MAX_OF_BOOKS 10

typedef struct
{
    char name[NAME_LENGTH];
    char author[AUTHOR_LENGTH];
    unsigned int year;
} BOOK;
int swapBooks(BOOK *lib, int b1, int b2)
{
    BOOK temp;
    temp = lib[b1];
    lib[b1] = lib[b2];
    lib[b2] = temp;
}
int printLib(BOOK *lib, int numOfBooks)
{
    int i;
    for (i = 0; i <= numOfBooks; i++)
        {
            printf("\"%s\" by %s, %d\n", lib[i].name, lib[i].author, lib[i].year);
        }
    putchar('\n');
}
int main()
{
    BOOK *lib;
    BOOK *temp;
    FILE *flib;
    int numOfBooks, i = 0, j;
    int curMaxOfBooks = DEFAULT_MAX_OF_BOOKS;
    int oldestId = 0, newestId = 0, smallestId = 0;
//������ �� �����
    flib = fopen("library.txt", "r");
    if (!(lib = (BOOK *)malloc((sizeof (BOOK)) * curMaxOfBooks)))
    {
        printf("Memory allocation error!");
        return 0;
    }
    while (fscanf(flib, "%s%s%d", &lib[i].name, &lib[i].author, &lib[i].year) != EOF)
    {
        i++;
        if (i == curMaxOfBooks)
        {
            curMaxOfBooks += 10;
            if (!(realloc(lib, (sizeof (BOOK)) * curMaxOfBooks)))
            {
                printf("Memory allocation error!");
                return 0;
            }

        }
    }
    fclose(flib);
    numOfBooks = i-1;
//����� ������ � ������ �� �����
    printLib(lib, numOfBooks);

// ����� ����� ������ � ����� ����� ����
    for (i = 0; i <= numOfBooks; i++)
    {
        if (lib[i].year > lib[newestId].year)
            newestId = i;
        if (lib[i].year < lib[oldestId].year)
            oldestId = i;
    }
    printf("Newest book - \"%s\" by %s, %d\nOldest book - \"%s\" by %s, %d\n\n",
           lib[newestId].name, lib[newestId].author, lib[newestId].year,
           lib[oldestId].name, lib[oldestId].author, lib[oldestId].year );

//��� ���������� ���� �� �������� ������� ���������� ���������� �������
    for (i = 0; i < numOfBooks; i++)
    {
        for (j = i; j <= numOfBooks; j++)
            if (strcmp(lib[j].author, lib[smallestId].author) < 0)
                smallestId = j;
        swapBooks(lib, i, smallestId);
    }
    printLib(lib, numOfBooks);
    free(lib);
    return 0;
}
